from flask import Flask, render_template, request
from flask import make_response
import requests, json

app = Flask(__name__)

token_url = "http://localhost:9080/auth/realms/cashfox/protocol/openid-connect/token"
consent_url = "http://localhost:9080/auth/realms/cashfox/protocol/openid-connect/auth?client_id=thirdparty&response_type=code"
client_id = "thirdparty"
client_secret = "ed56f607-b52c-43b5-b432-0b162b0ac32b"

@app.route('/')
def hello_world():
    return render_template('test_template.html', consent_url=consent_url)


@app.route('/callback/code')
def oauth_code_callback():
    app.logger.info(request.args)
    code = request.args["code"]
    access_token_response = get_access_token_by_code(code).json()
    access_token = access_token_response.get("access_token", None)
    inspection_response = inspect_token(access_token).json()
    token_info = inspect_token_offline(access_token)
    data = {
        "code":code,
        "token": access_token_response,
        "inspection": inspection_response,
        "token_info": token_info,
    }
    return render_template('success_template.html', **data)


def get_access_token_by_code(code: str = ...):
    data = {'grant_type': 'authorization_code', 'code': code}
    return requests.post(token_url, data=data, verify=False, auth=(client_id, client_secret))


def inspect_token(token):
    data = {'token': token}
    return requests.post(token_url + '/introspect', data=data, verify=False,
                             auth=(client_id, client_secret))


def inspect_token_offline(access_token):
    ...
    from keycloak import KeycloakOpenID
    keycloak_openid = KeycloakOpenID(server_url="http://localhost:9080/auth/",
                                     client_id=client_id,
                                     realm_name="cashfox",
                                     client_secret_key=client_secret)
    userinfo = keycloak_openid.userinfo(access_token)

    KEYCLOAK_PUBLIC_KEY = keycloak_openid.public_key()
    app.logger.info(KEYCLOAK_PUBLIC_KEY)
    pem_prefix = '-----BEGIN PUBLIC KEY-----\n'
    pem_suffix = '\n-----END PUBLIC KEY-----'
    key = '{}{}{}'.format(pem_prefix, KEYCLOAK_PUBLIC_KEY, pem_suffix)
    # aud false https://github.com/marcospereirampj/python-keycloak/issues/89
    options = {"verify_signature": True, "verify_aud": False, "verify_exp": True}
    token_info = keycloak_openid.decode_token(access_token, key=key, options=options)
    app.logger.info(token_info)
    return token_info


"""

List consent with admin-bearer
GET http://localhost:9080/auth/admin/realms/cashfox/users/30ef0eea-aa11-4fd6-b252-cf74e3cae15d/consents

Revoke consent, with admin-bearer
DELETE http://localhost:9080/auth/admin/realms/cashfox/users/30ef0eea-aa11-4fd6-b252-cf74e3cae15d/consents/thirdparty

Revoke consent with keycloak ui
http://localhost:9080/auth/realms/cashfox/account/#/applications

Create consent - without keycloak ui ??
POST http://localhost:9080/auth/realms/cashfox/login-actions/consent?client_id=thirdparty

"""

if __name__ == '__main__':
    app.run()